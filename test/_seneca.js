/* Prerequisites */
const Seneca = require('seneca');
const fs = require('fs');

module.exports = function (fin) {

    const seneca = Seneca({log: 'test'});

    seneca
    // activate unit test mode. Errors provide additional stack tracing context.
    // The fin callback is called when an error occurs anywhere.
        .test(fin)
        .use('basic')
        .use('entity')
        .use('../lib/client', {
            card: 'admin@sample-test',
            fs: fs,
            namespaces: 'always',
            multiuser: true
        });

    return seneca;
};