const should = require('should');
const test_seneca = require('./_seneca');

describe('composer-store', function() {
    describe('save$', function() {
        it('should save changes successfully into exists asset', function (done) {
            var seneca = test_seneca(done);

            seneca.ready(function(err){

                if(err) return done(err);

                const asset = seneca.make$('org.example.basic', 'SampleAsset');

                asset.assetId = '9';
                asset.value = 'newTestvalue123';
                asset.card$ = 'admin@sample-test';

                asset.save$(function (err, result) {
                        if(err) return done(err);

                        console.log(result);

                        result.should.not.be.empty();
                        done();
                    });
            })

        }).timeout(10000);

    });
});


