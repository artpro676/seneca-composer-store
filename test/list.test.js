const should = require('should');
const test_seneca = require('./_seneca');

describe('composer-store', function() {
    describe('list$', function() {
        it('should loads an array successfully', function (done) {
            var seneca = test_seneca(done);

            seneca.ready(function(err){
                if(err) return done(err);

                seneca
                    .make$('org.example.basic', 'SampleAsset')
                    .list$({
                        card$: 'admin@sample-test'
                    }, function (err, result) {
                        if(err) return done(err);
                        result.should.not.be.empty();
                        done();
                    });
            })

        }).timeout(10000);

    });
});


