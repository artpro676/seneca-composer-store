const should = require('should');
const test_seneca = require('./_seneca');

describe('composer-store', function () {
    describe('transaction$', function () {
        it('should invoke SampleTransaction successfully', function (done) {
            var seneca = test_seneca(done);

            seneca.ready((err) => {

                if(err) return done(err);

                seneca.transaction$({
                    $class: 'org.example.basic.SampleTransaction',
                    asset: 'resource:org.example.basic.SampleAsset#0',
                    newValue: 'Hello world',
                    card$: 'admin@sample-test'
                }, function (err, result) {
                    if (err) return done(err);
                    result.should.not.be.empty();
                    result.length.should.be.eql(64);
                    done();
                });
            })

        }).timeout(10000);
    });
});


