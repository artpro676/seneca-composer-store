'use strict';

var _ = require('lodash');

/*
native$ = object => use object as query, no meta settings
native$ = array => use first elem as query, second elem as meta settings
*/

class Utils {

    static idstr(obj) {
        return obj && obj.toHexString ? obj.toHexString() : '' + obj
    }

    static makeid(hexstr) {
        if (_.isString(hexstr) && 24 === hexstr.length) {
            try {
                return ObjectID.createFromHexString(hexstr)
            } catch (e) {
                return hexstr
            }
        }

        return hexstr
    }


    static fixquery(qent, q) {
        var qq = {}

        if (!q.native$) {
            if (_.isString(q)) {
                qq = {
                    _id: makeid(q)
                }
            } else if (_.isArray(q)) {
                qq = {
                    _id: {
                        $in: q.map(id => {
                            return makeid(id)
                        })
                    }
                }
            } else {
                for (var qp in q) {
                    if (!qp.match(/\$$/)) {
                        qq[qp] = q[qp]
                    }
                }
                if (qq.id) {
                    qq._id = makeid(qq.id)
                    delete qq.id
                }
            }
        } else {
            qq = _.isArray(q.native$) ? q.native$[0] : q.native$
        }

        return qq
    }

    static metaquery(qent, q) {
        var mq = {};

        if (!q.native$) {
            if (q.sort$) {
                for (var sf in q.sort$) break
                var sd = q.sort$[sf] < 0 ? 'descending' : 'ascending';
                mq.sort = [[sf, sd]]
            }

            if (q.limit$) {
                mq.limit = q.limit$ >= 0 ? q.limit$ : 0
            }

            if (q.skip$) {
                mq.skip = q.skip$ >= 0 ? q.skip$ : 0
            }

            if (q.fields$) {
                mq.fields = q.fields$
            }
        } else {
            mq = _.isArray(q.native$) ? q.native$[1] : mq
        }

        return mq
    }
}