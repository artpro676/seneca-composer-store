'use strict';

var _ = require('lodash');
var Utils = require('./utils');
var Adapter = require('./adapter');


var actionRole = 'hyperledger-composer';
var name = 'composer-store';

/*
native$ = object => use object as query, no meta settings
native$ = array => use first elem as query, second elem as meta settings
*/

module.exports = function (opts) {
    var seneca = this;
    var desc;

    var adapterInst = null;

    function error(args, err, cb) {
        if (err) {
            seneca.log.error('entity', err, {store: name});
            cb(err);
            return true
        } else return false
    }

    function configure(conf, cb) {
        adapterInst = new Adapter(conf);
        adapterInst
            .connect()
            .then(() => {
                adapterInst.discoverModelDefinitions(conf, cb)
            })
    }

    function parseArguments(args) {
        var q = {};
        var options = {};

        _.each(args.q || args, (v, k) => {

            if (_.last(k) === '$') {
                const newKey = k.replace('$', '');
                options[newKey] = v;
            } else {
                q[k] = v;
            }
        });

        return {q, options};
    }

    function getClass(ent) {
        const cannon = ent.canon$({object: true});
        return _.chain(cannon)
            .pick(['zone', 'base', 'name'])
            .values()
            .compact()
            .join('.')
            .value();
    }

    var store = {
        name: name,

        close: function (args, cb) {
            adapterInst.disconnect(cb);
        },

        save: function (args, cb) {
            var ent = args.ent;
            var entp = {
                $class: getClass(ent)
            };
            const fields = ent.fields$();
            fields.forEach(fieldName => entp[fieldName] = ent[fieldName]);

            console.log('>>>>>', args);

            cb(null, entp);
        },

        load: function (args, cb) {
            // var qent = args.qent;
            // var q = args.q;
            //
            // let fent = qent.make$(entp);
            // seneca.log.debug('load', q, fent, desc);
            console.log(args);

            cb(null, {});
        },

        list: function (args, cb) {
            var qent = args.qent;

            const {q, options} = parseArguments(args);

            if(!q.$class) {
                q.$class = getClass(qent)
            }

            adapterInst.all(q.$class, q, options, function (err, result) {
                if (err) return cb(err);
                cb(null, result);
            });
        },

        remove: function (args, cb) {
            var qent = args.qent;
            var q = args.q;
            console.log(args);
            cb(null, {});
        },

        native: function (args, done) {
            done(null, adapterInst);
        }
    };

    var meta = seneca.store.init(seneca, opts, store);
    desc = meta.desc;




    seneca.decorate('transaction$', function (args, cb) {

        const {q, options} = parseArguments(args);

        adapterInst.submitTransaction(q, options, function (err, result) {
            if (err) return cb(err);
            cb(null, result);
        });
    });

    seneca.add({init: store.name, tag: meta.tag}, function (args, done) {
        configure(opts, function (err) {
            if (err)
                return seneca.die('store', err, {store: store.name, desc: desc});
            return done()
        })
    });

    return {name: store.name, tag: meta.tag}
};